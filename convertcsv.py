from os import listdir
from os.path import isfile, join
import csv
import re


an='Name: ','name'
at='Title: ','title'
ap='Phone: ','phone'
ad1='Address1: ','address1'
ad2='Address2: ','address2'
ac='City: ','city'
ast='State: ','state'
az='Zip: ','zip'
ah='Hospital Affliation: ','hospital'
ar='Rating: ','rating'
al='Language Known: ','language'
asp='Speciality: ','specialty'
ab='Board Certificate: ','board'
ae='Education: ','education'
ai='Insurance: ','insuranace'
apm='Professional Membership: ','membership'
aa='Awards: ','awards'
aps='Professional Statement: ','profstatement'
arc="review commands:",'reviews'
    
docTuple = (an,at,ap,ad1,ad2, ac, ast, az, ah, ar, al, asp, ab, ae, ai, apm, aa, aps, arc)


class Doctor:
    def __init__(self):
        self.name =""
        self.title =""
        self.phone =""
        self.address1 =""
        self.address2 =""
        self.city =""
        self.state =""
        self.zip =""
        self.hospital =""
        self.rating =""
        self.language =""
        self.specialty =""
        self.board =""
        self.education =""
        self.insuranace =""
        self.membership =""
        self.awards =""
        self.profstatement =""
        self.reviews = ""

    def setAddress1(self, t):
        self.address1=t

    def setName (self, t):
        self.name = t
    
    def addReview (self, t):
        self.reviews = self.reviews+ " ***** "+t;
    
    def toCsv(self):
        row =[]
        for (ugly, pretty) in docTuple:
            row.append(getattr(self, pretty))
        return row
    
    def toString(self):
        #return self.name + self.address1 +self.city +self.language+ self.reviews
        return self.toCsv()


def convertFile1(fo, f):
    fname = f.split(".")[0]
    print fo+" "+f
    print fname
    return




def convertFile(fo, f):
        
    file = open(fo+f, 'rb')
    f = open(fo+(f.split(".")[0]+".csv"), 'wb')
    
    doc = Doctor()
    namepattern = re.compile("(^\d+ \. )(.*)")
    

    
    
    csvheaders =[];
    for (ugly, pretty) in docTuple:
        csvheaders.append(pretty)
    
    writer = csv.writer(f)
    writer.writerow( csvheaders )
    
    
    
    
    
    readReviews = False
    for line in file:
        if line.strip() == "":
            continue
        
        if line.startswith("---"):
            readReviews=False
            #print doc.toCsv()
            if not doc.name == "":
                writer.writerow( doc.toCsv() )
            doc = Doctor()
        if line.startswith(arc):
            readReviews=True;
        
        if readReviews == False:
            if namepattern.search(line):
                namearray = line.split(" . ")
                doc.setName(namearray[1].strip())
                
            for (ugly, pretty) in docTuple:
                if line.startswith(ugly):
                    setattr(doc, pretty, line.split(ugly)[1].strip())
                    #doc.setAddress1(line.split(ugly)[1])      
    
        if readReviews == True and not line.startswith(arc):
            review = line.strip()
            if not review =="":
                doc.addReview(review)
    
                
    writer.writerow( doc.toCsv() )

folderpath="E:/temp/py/CompleteData_17_8/"

for f in listdir(folderpath):
    if isfile(join(folderpath, f)):
        if f.endswith(".txt"):
            print folderpath+f
            fname = f.split(".")[0]
            if (isfile(fname+".csv")):
                print fname+".csv already exists. Skipping."
            else:
                print "converting "+f    
                convertFile(folderpath,f);


