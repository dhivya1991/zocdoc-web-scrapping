import requests
import json
from BeautifulSoup import BeautifulSoup
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
import MySQLdb
from string import punctuation
from operator import itemgetter
from time import sleep


def do_geocode(address):
	geolocator = Nominatim()
	try:
		return geolocator.geocode(address)
	except:
		print 'do_geocode'
		return None
def do_getURL(url):
	try:
		response = requests.get(url)
		html = response.content
	except:
		sleep(5)
		print 'getURL'
		do_getURL(url)
	return BeautifulSoup(html)
def do_getURLWithoutBeautifulSoup(url):
	try:
		response = requests.get(url)
	except:
		sleep(5)
		print 'getURLwithout'
		do_getURLWithoutBeautifulSoup(url)
	return response.content
def getConnection():
	return MySQLdb.connect(host="localhost", user = "root", passwd = "dhivya1991@", db = "doctorsinfo")
def saveDoctor(doctorName,doctorTitle,doctorPhone,doctorAddress1,doctorAddress2,doctorCity,doctorState,doctorZip,doctorOverAllRating,doctorHospitalAffliation,doctorLanguageKnown,doctorSpeciality):
	try:
		db = getConnection()
		cursor = db.cursor()
		query ='INSERT INTO doctors (name,title,phone,address1,address2,city,state,zip,overAllRating,hospitalAffliation,language,speciality) VALUES ('+"'"+doctorName+"','"+doctorTitle+"','"+doctorPhone+"','"+doctorAddress1+"','"+doctorAddress2+"','"+doctorCity+"','"+doctorState+"','"+doctorZip+"','"+doctorOverAllRating+"','"+doctorHospitalAffliation+"','"+doctorLanguageKnown+"','"+doctorSpeciality+"'"+')'
		print query
		cursor.execute(query)
		db.commit()
	except:
		print "SQL Error"
		db.close()
	db.close()
def saveReviews(reviewList,doctorName):
	try:
		db =getConnection()
		cursor = db.cursor()
		query='INSERT INTO reviews (name,review) VALUES'
		i=0;
		for reviewItem in reviewList:
			if i is (len(reviewList)-1):
				query=query+"('"+doctorName+"','"+reviewList[i]+"')"
			else:	
				query=query+"('"+doctorName+"','"+reviewList[i]+"'),"
			i+=1
		print query
		cursor.execute(query)
		db.commit()
	except:
		print "SQL Error"
		db.close()
	db.close()

wholeContent=do_getURL('https://www.zocdoc.com/morespecialties')	
cityListDiv=wholeContent.find('div', {'class' : 'findby-item-links findby-open'})
#file = open('testFile.txt','w') 
#linkNameFile = open('linkName.txt','w')
resonToVisit = [75,83,1309,1063,422,2970,130,365,76,494,1812,122,2649,1175,194,3210,3193,1462,3194,121,1019,1705,134,3316,109,1204,125,2620,1023,1801,124,3315,1177,3213,1854,1176,1886,1889,2551,2728,1207,2164,2204,2212,2237,2263,2501,2304,2305,78,77,504,150,3219,161,2366,123,1487,1488,1029,2522,1550,2641,421,2642 ];
doctorURLArray=[]
doctorName=''
doctorTitle=''
doctorPhone=''
doctorAddress1=''
doctorAddress2=''
doctorCity=''
doctorState=''
doctorZip=''
doctorOverAllRating=''
doctorHospitalAffliation=''
doctorLanguageKnown=''
doctorSpeciality=''
reviewList=[]
doctorList=[]

for row in cityListDiv.findAll('li'):
	anchor=row.find('a')
	
	if anchor.has_key('href'):
		cityURL='https://www.zocdoc.com'+anchor['href']
		currentPage=do_getURL(cityURL)
		paginationContent=currentPage.find('div',{'class':'sg-title'})
		#linkNameFile.write('*******************\n')
		#linkNameFile.write(cityURL)
		for j in resonToVisit:
			i=0
			for eachPage in paginationContent.findAll('a'):
			
				pageURL='https://www.zocdoc.com/search/searchresults?SpecialtyId=153&IsSearchingByName=false&'+eachPage['href'].split('&')[1]+'&InsuranceId=-1&InsurancePlanId=-1&ProcedureId='+str(j)+'&ProcedureChanged=true&Gender=-1&DayFilter=0&LanguageId=-1&LanguageChanged=false&TimeFilter=AnyTime&PatientTypeChild=&SortSelection=0&HospitalId=-1&DirectoryType=&Offset='+str(i)+'&ReferrerType=&SubmitSearchClicked=false&ShowChooseLaterOption=true&HasNoSearchResults=false&SearchQueryGuid=6369ee13-9633-4db6-98b0-eb484410ca7c&_=1501032953383'
				#linkNameFile.write('\n'+pageURL)
				i=i+10
		
				html = do_getURLWithoutBeautifulSoup(pageURL)
				json_content= html.replace('for(;;);','')
				json_object = json.loads(json_content)
			
				for eachDoctor in json_object['model']['Doctors']:
					
					doctorUniqueLink=eachDoctor['ProfUrlForBookOnline'].split('reason_visit')[0]
					
					if doctorUniqueLink  not in doctorURLArray:
						
						doctorURLArray.append(doctorUniqueLink)
						doctorName=eachDoctor['LongProfessionalName'].replace("'","''").encode('ascii','ignore')
						doctorTitle=eachDoctor['Title']
						if eachDoctor['PhoneNumber'] is not None:
							doctorPhone=eachDoctor['PhoneNumber']
						doctorAddress1=eachDoctor['Address1'].replace("'","''")
						doctorAddress2=eachDoctor['Address2'].replace("'","''")
						doctorCity=eachDoctor['City']
						doctorState=eachDoctor['State']
						doctorZip=eachDoctor['Zip']
						if eachDoctor['Rating'] is not None:
							doctorOverAllRating=str(eachDoctor['Rating']['RoundedRating'])
						#print eachDoctor['DisplaySpectialtyName']
						#print eachDoctor['ProfUrlForBookOnline']
						doctorURL="https://www.zocdoc.com"+eachDoctor['ProfUrlForBookOnline']						
						#linkNameFile.write('\n'+doctorURL)
						doctordetialedInfo = do_getURL(doctorURL)
						#file.write(eachDoctor['LongProfessionalName']+'\n') 
						HospitalAffliation=languageKnow=specialityField=''
						
						for hospitalAffliation in doctordetialedInfo.findAll('li',{'class':'sg-para3 sg-navy hospitalAffiliations'}):
							HospitalAffliation= hospitalAffliation.text.replace("'","''")+';'+HospitalAffliation
						for language in doctordetialedInfo.findAll('li',{'class':'sg-para3 sg-navy language'}):
							languageKnow=language.text+';'+languageKnow
						for speciality in doctordetialedInfo.findAll('li',{'class':'sg-para3 sg-navy specialty'}):
							specialityField=speciality.text+';'+specialityField
						doctorHospitalAffliation=HospitalAffliation
						doctorLanguageKnown=languageKnow
						doctorSpeciality=specialityField
						reviewFile = open('reviewFile.txt','w')
						for review in doctordetialedInfo.findAll('p',{'itemprop':'reviewBody'}):
							reviewFile.write( review.text.replace("'","''").encode('ascii','ignore'))
						
						reviewFile.close()
						
							
						location =do_geocode(eachDoctor['Address1']) 
						if location is not None:
							doctorHealthGrades='https://www.healthgrades.com/api3/usearch?userLocalTime=23%3A59&what='+eachDoctor['LongProfessionalName']+'&where='+eachDoctor['City']+' '+eachDoctor['State']+'&pt='+str(location.latitude)+', '+str(location.longitude)+'&sort.provider=bestmatch&categories=1&sessionId=Ses4c5ihj5l4t387&requestId=Rde18228d0bf60a6b&pageSize.provider=36&pageNum=1&isFirstRequest=true'
							
							#linkNameFile.write('\n'+doctorHealthGrades)
							
							html = do_getURLWithoutBeautifulSoup(doctorHealthGrades) 
							json_object = json.loads(html)
							
							for eachResult in json_object['search']['searchResults']['provider']['results']:
								
								doctorReviewHealthGrades='https://www.healthgrades.com'+eachResult['providerUrl']
								#linkNameFile.write('\n'+doctorReviewHealthGrades)
								content=do_getURL(doctorReviewHealthGrades)
								
								for eachComment in content.findAll('div',{'class':'comment-text'}):
									reviewList.append((eachComment.find('span').text.replace("'","''")).encode('ascii','ignore'))
									#doctorList.append(doctorName)
								if len(reviewList) > 0:
									saveReviews(reviewList,doctorName)
								break  # confirm this break should be there to get the first search result alone
						saveDoctor(doctorName,doctorTitle,doctorPhone,doctorAddress1,doctorAddress2,doctorCity,doctorState,doctorZip,doctorOverAllRating,doctorHospitalAffliation,doctorLanguageKnown,doctorSpeciality)
							
							
					#break
				
				
			
			#linkNameFile.write('________________________________________\n')
			
	
	
#file.close()
#linkNameFile.close()
