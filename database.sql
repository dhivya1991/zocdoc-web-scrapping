drop DATABASE DoctorsInfo;
create DATABASE DoctorsInfo;

drop table reviews;
drop table doctors;

CREATE TABLE doctors (
    id int,
    name varchar(255),
    Title varchar(10),
    phone varchar(20),
    address1 varchar(255),
	address2 varchar(255),
	city varchar(50),
	state varchar(10),
	zip int(10),
	overAllRating varchar(10),
	hospitalAffliation varchar(255),
	language varchar(255),
	speciality varchar(100)
	
);

create Table reviews(
	Name varchar(255) ,
	review varchar(1000)
);